# Simple NodeJS Webapp

The application uses an environment variable `APPLICATION_INSTANCE`.  
This variable will be used by the application to listen on a specific path (i.e. `/${application_instance}/health`).

## Launch the application:

```bash
export APPLICATION_INSTANCE=example
node src/count-server.js
```

You can access the application for now on the minikube IP and the port of the service or using the ingress with count-server. In order to do that you have to set the minikube ip for your host (count-server) before.

You can increment the counter on the application like this:

```bash
curl http://count-server:30832/example/increment
```

The application is scaled by adding more replicas.
