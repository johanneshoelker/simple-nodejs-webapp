# Q1:

A Microservice architecture consists of several independent programs/services. They can run on their own and use APIs to communicate to the other services. Most often they are deployed in containers (but this is not always the case), which run independently. A Monolithic application however consists of one big program, whcih is not seperated and therefore only runs with all of its components at once.

# Q2:

## Microservices:

### Pros:

- working in teams is easier because of seperate applications
- easy scalable
- better maintanable
- 

### Cons:

- needs to be organised (orchestration)
- 

## Monolithic:

### Pros:

- runs without any orchestration tool
- uses less resources because it can be designed for specific hardware
- 

### Cons:

- if one part breaks the whole application does not run anymore
- working on it requires knowing the whole application with every part of it

# Q3:

It should be splitted Full-Stack which means one team should do the Front and Backend of their Feature. This means one team has experts of Frontend and Backend and their can share their skills in order to focus on one single feature.

# Q4:

In the CAP theorem there is the idea, that you cannot combine the three aspects of software at the same time: Consistency, Availability, Partition Tolerance

In Microservices you always have availability and partition tolerance but the consistency can be lacking. This is due to the fact that the orchestration can handle scaling the containers but there is always some downtime or other problems which make it less consistent.

# Q5:

For Microservices this is important to know because for architecting a software you should know before what you need the most and what you can leave out.

If the application shall be available all the time and should be consistent too, it is not able to handle hardware problems and failures for example. THis is hard to gain in a microservices architecture

# Q6:

Scling can be done by only adding more replicas or by more advanced services which listen on the demand and scale the resources according to the needs. 

# Q7:

When the application is not storing any state it is called stateless. It means that every newly started service is starting without knowing what happened before. This is important for microservices because it provides the ability to restart the application easily and to be flexible where and when the application is running.

# Q8:

To communicate with the services and send/receive data we need an API Gateway. It acts as the middleperson between the cluster and the outside. It is managing security/authorization and the routing to the respective pods.
